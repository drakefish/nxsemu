﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NxsEmu.Network.GamePackets;
using NxsEmu.Game;
using NxsEmu.Game.Gateway;

namespace NxsAuthServer
{
    [PacketDescription(GamePacketMsg.CGatewayHello)]
    public class CGatewayRequest : GamePacket<CGatewayRequest>
    {
        [Field(32)]
        public UInt32 accountId;
        [ArrayField(16), Field(8)]
        public Byte[] gatewayTicket;
        [StringField(true)]
        public String accountName;
    }

    [PacketDescription(GamePacketMsg.SCharactersList)]
    public class SCharactersList : GamePacket<SCharactersList>
    {
        [Field(32)]
        public uint unknown; // this is a new field, I'm not sure what it is about.
        [Field(32)]
        public uint characterCount;
        [ArrayField("characterCount"), StructField()]
        public CharacterListEntry[] characterInfo; // tmp, this is another struct
        // the following has changed I think. There seem to be more fields than just a uint.
        [Field(32)]
        public uint additionalCount;
        [ArrayField("additionalCount"), Field(32)]
        public uint[] additionalAllowedCharCreations;
        [Field(32)]
        public uint unknown2;
        [Field(32)]
        public uint unknown3;
    }

    [PacketDescription(GamePacketMsg.SJoinWorld1)]
    public class SJoinWorld1 : GamePacket<SJoinWorld1>
    {
        // possibly map id
        [Field(32)]
        public uint unknown;
        [StructField]
        public WorldLocation worldLocation;
    }

    [PacketDescription(GamePacketMsg.SJoinWorldUnk2)]
    public class SJoinWorldUnk2 : GamePacket<SJoinWorldUnk2>
    {
        [Field(32)]
        public uint unk0;
        [Field(32)]
        public uint unk1;
        [Field(32)]
        public uint unk2;
    }

    [PacketDescription(GamePacketMsg.SJoinWorldUnk3)]
    public class SJoinWorldUnk3 : GamePacket<SJoinWorldUnk3>
    {
        [Field(32)]
        public uint unk;
    }

    [PacketDescription(GamePacketMsg.SJoinWorldUnk4)]
    public class SJoinWorldUnk4 : GamePacket<SJoinWorldUnk4>
    {
        [Field(32)]
        public uint count;
        [ArrayField("count"), StructField]
        SJoinWorldUnk4_struct[] unkArray;

        public struct SJoinWorldUnk4_struct
        {
            [Field(32)]
            public uint unk0;
            [Field(32)]
            public uint unk1;
            [Field(32)]
            public uint unk2;
            [Field(32)]
            public uint unk3;
            [Field(32)]
            public Single unk4; // ?
            [Field(32)]
            public uint unk5;
        }
    }

    [PacketDescription(GamePacketMsg.SJoinWorldUnk5)]
    public class SJoinWorldUnk5 : GamePacket<SJoinWorldUnk5>
    {
        [Field(32)]
        public uint unk0;
        [Field(32)]
        public uint unk1;
    }

    [PacketDescription(GamePacketMsg.SJoinWorldUnk6)]
    public class SJoinWorldUnk6 : GamePacket<SJoinWorldUnk6>
    {
        [Field(64)]
        public ulong unk0;
        [Field(64)]
        public ulong unk1;
        [Field(32)]
        public uint unk2;

        // these two are under another structure, maybe theyre used elsewhere
        [Field(9)]
        public uint unk3;
        [Field(32)]
        public uint unk4;

        [Field(32)]
        public uint unk5;
        [Field(32)]
        public uint unk6;
        [Field(64)]
        public ulong unk7;
        [Field(32)]
        public uint unk8;
        [Field(64)]
        public ulong unk9;
        [Field(32)]
        public uint unk10;
        [Field(32)]
        public uint unk11;
        [Field(32)]
        public uint unk12;
        [Field(32)]
        public uint unk13;

        [Field(32)]
        public uint count0;
        [ArrayField("count0"), Field(32)]
        public uint[] unkArray0;

        [Field(32)]
        public uint count1;
        [ArrayField("count1"), Field(32)]
        public uint[] unkArray1;

        [Field(5)]
        public byte unk14;
    }

    /*public struct CharacterListEntry
    {
        [Field(64)]
        public ulong characterId;
        [StringField(true)]
        public string characterName;
        [Field(2)]
        public uint characterSex;
        [Field(5)]
        public uint characterRace;
        [Field(5)]
        public uint characterClass;
        [Field(32)]
        public uint characterFaction;
        [Field(32)]
        public uint characterLevel;
        [Field(32)]
        public uint bodyCount;
        [ArrayField("bodyCount"), StructField()]
        public CharacterEquipment[] bodyVisuals;
        [Field(32)]
        public uint equipCount;
        [ArrayField("equipCount"), StructField()]
        public CharacterEquipment[] equipmentVisuals; // NOTE: This is possibly at the end? The packet is weird.


        [Field(32)]
        public uint unk0;
        [Field(32)]
        public uint unk1; // worldID?
        [Field(32)]
        public uint unk2; // realmId?
        [Field(32)]
        public uint unk3; // isntanceID?
        [StructField()]
        public WorldLocation worldLocation;
        [Field(3)]
        public uint charactedPath;
        [Field(1)]
        public uint unk4;
        [Field(32)]
        public uint unk5;

        [Field(4)]
        public uint unkCount;
        [ArrayField("unkCount"), Field(32)]
        public uint[] unkArray0;
        [ArrayField("unkCount"), Field(32)]
        public uint[] unkArray1;

        [Field(32)]
        public uint boneCustomizationCount;
        [ArrayField("boneCustomizationCount"), Field(32)]
        public Single[] boneCustomizations;

        [Field(32)]
        public Single unk6;
    }*/

    /*public struct CharacterEquipment
    {
        [Field(7)]
        public uint itemSlot;
        [Field(32)]
        public uint itemDisplayId;
        [Field(32)]
        public uint itemColorSetId;
        [Field(32)]
        public uint itemDyeData;
    }*/

    /*public struct WorldLocation
    {
        [StructField()]
        public Position location;
        [Field(32)]
        public float yaw;
        [Field(32)]
        public float pitch;
        [Field(32)]
        public uint tileId;
    }

    // todo: make our own Vector3 implementation use this.
    public struct Position
    {
        [Field(32)]
        public float x;
        [Field(32)]
        public float y;
        [Field(32)]
        public float z;
    }*/

    [PacketDescription(GamePacketMsg.SCreatureSpawnInfo)]
    public class SCreatureSpawnInfo : GamePacket<SCreatureSpawnInfo>
    {
        [Field(32)]
        public uint unitId;
        [Field(6)]
        public uint unitType;
        [UnionField("unitType")]
        public UnionCreatureData unitInfo;
        [Field(32)]
        public uint createFlags;
        [Field(32)]
        public uint statCount;
        [ArrayField("statCount"), StructField]
        public StatValueChange[] stats;
        [Field(32)]
        public uint time;
        [Field(16)]
        public ushort commandCount;
        [ArrayField("commandCount"), StructField]
        public CreatureCommand[] commands;
        [Field(32)]
        public uint propertyCount;
        [ArrayField("propertyCount"), StructField]
        public CreatureProperty[] properties;
        [Field(32)]
        public uint equipmentCount;
        [ArrayField("equipmentCount"), StructField]
        public CharacterEquipment[] equipment;
        [Field(16)]
        public ushort spellCastInitCount;
        [ArrayField("spellCastInitCount"), StructField()]
        public SpellCast[] spellInit;
        [Field(32)]
        public uint currentSpellUniqueId;
        [Field(32)]
        public uint factionId;
        [Field(32)]
        public uint unitTagOwner;
        [Field(64)]
        public ulong groupTagOwner;
        [Field(32)]
        public uint timeSinceDeath;
        [Field(64)]
        public ulong activePropId;
        [Field(32)]
        public uint socketId;
        [Field(32)]
        public uint tileId;
        [Field(32)]
        public uint clusterId;
        [Field(32)]
        public uint creatureSetId;
        [Field(32)]
        public uint lootFlags;
        [Field(32)]
        public uint vendorPrereqId;
        [Field(32)]
        public uint characterTitleId;
        [Field(32)]
        public uint miniMapMarker;
        [Field(32)]
        public uint displayInfoId;
        [Field(32)]
        public uint outfitInfoId;
    }


    

    public struct StatValueChange
    {
        [Field(5)]
        public uint stat;
        [Field(32)]
        public uint newValue;
        [Field(32)]
        public float newFloatValue;
        [Field(32)]
        public uint data;
        [Field(5)]
        public uint reason;
    }

    // this was bigger before, they removed modifiers from it.
    // should be complete.
    public struct CreatureProperty
    {
        [Field(8)]
        public uint propertyId;
        [Field(32)]
        public Single _base;
        [Field(32)]
        public Single value;
    }

    public struct SpellCast
    {
        [Field(32)]
        public uint casterId;
        [Field(32)]
        public uint originalTargetId;
        [Field(32)]
        public uint serverUniqueId;
        [Field(32)]
        public uint spellId;
        [Field(1)]
        public Boolean unk0;
        [Field(32)]
        public uint targetCount;
        [ArrayField("targetCount"), StructField()]
        public SpellTargetInfo[] targetInfo;
        [Field(8)]
        public byte initialPositionCount;
        [ArrayField("initialPositionCount"), StructField()]
        public SpellInitialPosition[] initialPositions;
        [Field(8)]
        public byte telegraphPositionCount;
        [ArrayField("telegraphPositionCount"), StructField()]
        public TelegraphPosition[] telegraphPositions;
    }

    public struct SpellTargetInfo
    {
        [Field(32)]
        public uint unitId;
        [Field(8)]
        public byte ndx;
        [Field(8)]
        public byte targetFlags;
        [Field(16)]
        public ushort unk0;
        [Field(4)]
        public uint combatResult;
        [Field(8)]
        public byte effectCount;
        [ArrayField("effectCount"), StructField]
        public SpellEffectInfo[] effectInfo;
    }

    public struct SpellEffectInfo
    {
        [Field(32)]
        public uint effectId;
        [Field(32)]
        public uint effectUniqueId;
        [Field(32)]
        public uint delayTime;
        [Field(32)]
        public uint timeRemaining;
        [Field(2)]
        public uint infoType;
        [UnionField("infoType")]
        public UnionSpellEffectInfo info;
    }

    public struct SpellInitialPosition
    {
        [Field(32)]
        public uint unitId;
        [Field(32)]
        public uint targetFlags;
        [StructField()]
        public Position initialPosition;
        [Field(32)]
        public float initialYaw;
    }

    public struct TelegraphPosition
    {
        [Field(32)]
        public uint telegraphId;
        [Field(32)]
        public uint attachedUnitId;
        [Field(32)]
        public uint targetFlags;
        [StructField()]
        public Position position;
        [Field(32)]
        public float yaw;
    }

    public struct CreatureCommand
    {
        [Field(5)]
        public uint type;
        [UnionField("type")]
        public UnionCreatureCommand data;
    }

    public struct EmptyUnion
    {

    }

    // complete
    public struct CreatureSimple
    {
        [Field(32)]
        public uint creatureId;
        [Field(32)]
        public uint questChecklistIdx;
    }

    // complete (naming/tweaking needed)
    public struct CreaturePlayer
    {
        [Field(64)]
        public UInt64 characterId;
        [Field(32)]
        public UInt32 realmId;
        [StringField(true)]
        public string name;
        [Field(5)]
        public uint raceId;
        [Field(5)]
        public uint classId;
        [Field(2)]
        public uint sex;
        [Field(64)]
        public ulong groupId;
        [Field(32)]
        public uint petCount;
        [ArrayField("petCount"), Field(32)]
        public uint[] petIdList;
        [StringField(true)]
        public string guildName;
        [Field(3)]
        public uint guildType;
        [Field(8)]
        public byte guildCount;
        [ArrayField("guildCount"), Field(64)]
        public ulong[] guildIds;
        [ArrayField(4), StructField]
        public UnkPlayerStruct0[] unkArray; // possibly 4 times the struct, not necessarily an array.
        [Field(16)]
        public ushort boneCustomizationCount;
        [ArrayField("boneCustomizationCount"), Field(32)]
        public float[] boneCustomizationList;
        [Field(32)]
        public uint pvpFlags;
    }

    // Possibly guilds-related? (maybe emblem stuff)
    public struct UnkPlayerStruct0
    {
        [Field(32)]
        public UInt32 unk0;
        [Field(32)]
        public UInt32 unk1;
        [Field(32)]
        public UInt32 unk2;
        [Field(32)]
        public UInt32 unk3;
    }

    public struct UnionCreatureData
    {
        [StructField()]
        public EmptyUnion nonPlayer;
        [StructField()]
        public EmptyUnion chest;
        [StructField()]
        public EmptyUnion destructible;
        [StructField()]
        public EmptyUnion vehicle;
        [StructField()]
        public EmptyUnion door;
        [StructField()]
        public EmptyUnion harvestUnit;
        [StructField()]
        public EmptyUnion corpseUnit;
        [StructField()]
        public EmptyUnion mount;
        [StructField()]
        public EmptyUnion collectableUnit;
        [StructField()]
        public EmptyUnion taxi;
        [StructField()]
        public EmptyUnion simple;
        [StructField()]
        public EmptyUnion platform;
        [StructField()]
        public EmptyUnion mailBox;
        [StructField()]
        public EmptyUnion aiTurret;
        [StructField()]
        public EmptyUnion instancePortal;
        [StructField()]
        public EmptyUnion plug;
        [StructField()]
        public EmptyUnion residence;
        [StructField()]
        public EmptyUnion structuredPlug;
        [StructField()]
        public EmptyUnion pinataLoot;
        [StructField()]
        public EmptyUnion bindPoint;
        [StructField()]
        public CreaturePlayer player;
        [StructField()]
        public EmptyUnion hidden;
        [StructField()]
        public EmptyUnion trigger;
        [StructField()]
        public EmptyUnion ghost;
        [StructField()]
        public EmptyUnion pet;
        [StructField()]
        public EmptyUnion esperPet;
        [StructField()]
        public EmptyUnion worldUnit;
        [StructField()]
        public EmptyUnion scannerUnit;
        [StructField()]
        public EmptyUnion camera;
        [StructField()]
        public EmptyUnion trap;
        [StructField()]
        public EmptyUnion destructibledoor;
        [StructField()]
        public EmptyUnion pickup;
        [StructField()]
        public EmptyUnion simpleCollidable;
    }

    public struct CreatureCommand_setPosition
    {
        [StructField()]
        public Position position;
        [Field(1)]
        public Boolean blend;
    }

    public struct UnionCreatureCommand
    {
        [StructField()]
        public EmptyUnion setTime;
        [StructField()]
        public EmptyUnion setPlatform;
        [StructField()]
        public CreatureCommand_setPosition setPosition;
        [StructField()]
        public EmptyUnion setPositionKeys;
        [StructField()]
        public EmptyUnion setPositionPath;
        [StructField()]
        public EmptyUnion setPositionSpline;
        [StructField()]
        public EmptyUnion setPositionMultiSpline;
        [StructField()]
        public EmptyUnion setPositionProjectile;
        [StructField()]
        public EmptyUnion setVelocity;
        [StructField()]
        public EmptyUnion setVelocityKeys;
        [StructField()]
        public EmptyUnion setVelocityDefault;
        [StructField()]
        public EmptyUnion setMove;
        [StructField()]
        public EmptyUnion setMoveKeys;
        [StructField()]
        public EmptyUnion setMoveDefault;
        [StructField()]
        public EmptyUnion setRotation;
        [StructField()]
        public EmptyUnion setRotationKeys;
        [StructField()]
        public EmptyUnion setRotationSpline;
        [StructField()]
        public EmptyUnion setRotationMultiSpline;
        [StructField()]
        public EmptyUnion setRotationFaceUnit;
        [StructField()]
        public EmptyUnion setRotationFacePosition;
        [StructField()]
        public EmptyUnion setRotationSpin;
        [StructField()]
        public EmptyUnion setRotationDefault;
        [StructField()]
        public EmptyUnion setScale;
        [StructField()]
        public EmptyUnion setScaleKeys;
        [StructField()]
        public EmptyUnion setState;
        [StructField()]
        public EmptyUnion setStateKeys;
        [StructField()]
        public EmptyUnion setStateDefault;
        [StructField()]
        public EmptyUnion setMode;
        [StructField()]
        public EmptyUnion setModeKeys;
        [StructField()]
        public EmptyUnion setModeDefault;
    }

    public struct SpellDamageInfo
    {
        [Field(32)]
        public uint rawDamage;
        [Field(32)]
        public uint rawScaledDamage;
        [Field(32)]
        public uint absorbedAmount;
        [Field(32)]
        public uint shieldAbsorbAmount;
        [Field(32)]
        public uint adjustedDamage;
        [Field(32)]
        public uint unk;// possibly at the beginning instead (means the previous fields could be shifted)
        [Field(1)]
        public bool killedTarget;
        [Field(4)]
        public uint combatResult;
        [Field(2)]
        public uint damageType;
    }

    public struct UnionSpellEffectInfo
    {
        [StructField]
        public EmptyUnion empty;
        [StructField]
        public SpellDamageInfo damageDesc;
    }
}
