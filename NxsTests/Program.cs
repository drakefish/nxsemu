﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using NxsEmu;
using NxsEmu.Cryptography;
using System.Numerics;

namespace NxsTests
{
    class Program
    {
        static void Main(string[] args)
        {
            //PacketCryptSpeedTest();
            //HashTest();
            BigIntegersTest();

            Console.ReadLine();
        }

        static void BigIntegersTest()
        {
            BigInteger N_BigInteger = new BigInteger(new Byte[] {
                0xE3, 0x06, 0xEB, 0xC0, 0x2F, 0x1D, 0xC6, 0x9F, 0x5B, 0x43, 0x76, 0x83, 0xFE, 0x38, 0x51, 0xFD,
                0x9A, 0xAA, 0x6E, 0x97, 0xF4, 0xCB, 0xD4, 0x2F, 0xC0, 0x6C, 0x72, 0x05, 0x3C, 0xBC, 0xED, 0x68,
                0xEC, 0x57, 0x0E, 0x66, 0x66, 0xF5, 0x29, 0xC5, 0x85, 0x18, 0xCF, 0x7B, 0x29, 0x9B, 0x55, 0x82,
                0x49, 0x5D, 0xB1, 0x69, 0xAD, 0xF4, 0x8E, 0xCE, 0xB6, 0xD6, 0x54, 0x61, 0xB4, 0xD7, 0xC7, 0x5D,
                0xD1, 0xDA, 0x89, 0x60, 0x1D, 0x5C, 0x49, 0x8E, 0xE4, 0x8B, 0xB9, 0x50, 0xE2, 0xD8, 0xD5, 0xE0,
                0xE0, 0xC6, 0x92, 0xD6, 0x13, 0x48, 0x3B, 0x38, 0xD3, 0x81, 0xEA, 0x96, 0x74, 0xDF, 0x74, 0xD6,
                0x76, 0x65, 0x25, 0x9C, 0x4C, 0x31, 0xA2, 0x9E, 0x0B, 0x3C, 0xFF, 0x75, 0x87, 0x61, 0x72, 0x60,
                0xE8, 0xC5, 0x8F, 0xFA, 0x0A, 0xF8, 0x33, 0x9C, 0xD6, 0x8D, 0xB3, 0xAD, 0xB9, 0x0A, 0xAF, 0xEE, 0x00});
            
            Console.WriteLine(N_BigInteger.ToString());
        }

        static void HashTest()
        {
            Stopwatch watch = new Stopwatch();
            int blockSize = 1000;
            int iterations = 10000;

            var netSha = System.Security.Cryptography.SHA256.Create();
            var managedSha = System.Security.Cryptography.SHA256Managed.Create();
            SHA256_2 sha;

            Byte[] text1 = UTF8Encoding.ASCII.GetBytes("abc");
            Byte[] text2 = UTF8Encoding.ASCII.GetBytes("abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq");
            Byte[] text3 = UTF8Encoding.ASCII.GetBytes("aaaaaaaaaa");

            Byte[] buffer = new Byte[blockSize];
            for (int i = 0; i < blockSize; i++)
                buffer[i] = (byte)(i % 0x100);

            Byte[] hashBuffer = new Byte[32];

            watch.Reset();
            watch.Start();
            sha = new SHA256_2();
            for (int i = 0; i < iterations; i++)
            {
                sha.Update(buffer, 0, buffer.Length);
                sha.Final(hashBuffer);
            }
            watch.Stop();
            PrintResult("My Hash (full)", watch);

            watch.Reset();
            watch.Start();
            for (int i = 0; i < iterations; i++)
                netSha.ComputeHash(buffer);
            watch.Stop();
            PrintResult(".NET Hash (full)", watch);

            watch.Reset();
            watch.Start();
            for (int i = 0; i < iterations; i++)
                managedSha.ComputeHash(buffer);
            watch.Stop();
            PrintResult(".NET ManagedHash (full)", watch);
            //Logs.LogBuffer(hashBuffer);

            //Logs.LogBuffer(netSha.ComputeHash(text1, 0, text1.Length));

            watch.Reset();
            watch.Start();
            sha = new SHA256_2();

            for (int i = 0; i < iterations; i++)
                sha.Update(buffer, 0, buffer.Length);

            sha.Final(hashBuffer);
            watch.Stop();
            PrintResult("My Hash (update)", watch);

            watch.Reset();
            watch.Start();
            /*for (int i = 0; i < iterations; i++)
                netSha.TransformBlock(buffer, 0, buffer.Length, hashBuffer, 0);
            netSha.TransformFinalBlock(buffer, 0, buffer.Length);*/

            // ? I'm not iterating properly here I think
            int offset = 0;
            //while (buffer.Length - offset >= buffer.Length)
            for (int i = 0; i < iterations; i++)
                offset += netSha.TransformBlock(buffer, 0, buffer.Length, buffer, 0);
            netSha.TransformFinalBlock(buffer, 0, buffer.Length);
            watch.Stop();
            PrintResult(".NET Hash (update)", watch);

            watch.Reset();
            watch.Start();
            // same
            offset = 0;
            for (int i = 0; i < iterations; i++)
                offset += managedSha.TransformBlock(buffer, 0, buffer.Length, buffer, 0);
            managedSha.TransformFinalBlock(buffer, 0, buffer.Length); 
            watch.Stop();
            PrintResult(".NET ManagedHash (update)", watch);

            /*sha = new SHA256();
            sha.Update(text2, 0, text2.Length);
            sha.Final(hashBuffer);
            Logs.LogBuffer(hashBuffer);

            sha = new SHA256();
            for (int i = 0; i < 100000; i++)
                sha.Update(text3, 0, text3.Length);
            sha.Final(hashBuffer);
            Logs.LogBuffer(hashBuffer);
            */
        }

        //static Byte[] BigIntHelper_MakeWithPadding(

        static void PacketCryptSpeedTest()
        {
            int arrayLen = 1000;
            int loopCount = 1000;

            PacketCrypt crypt = new PacketCrypt(0x4E49EBFEDF603F15);
            Byte[] array = new Byte[arrayLen];

            // init to some value
            for (int i = 0; i < array.Length; i++)
                array[i] = (byte)(i % 0x100);

            // make sure it's JIT'd
            crypt.Decrypt(array, 0);
            crypt.DecryptBuffer(array, 0, 0);
            //crypt.DecryptBufferUnsafe(array, 0, 0);
            crypt.Encrypt(array, 0);
            crypt.EncryptBuffer(array, 0, 0);
            crypt.EncryptBufferUnsafe(array, 0, 0);

            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Reset();
            stopwatch.Start();
            for (int i = 0; i < loopCount; i++)
                crypt.Decrypt(array, array.Length);
            stopwatch.Stop();

            PrintResult("Decrypt", stopwatch);

            stopwatch.Reset();
            stopwatch.Start();
            for (int i = 0; i < loopCount; i++)
                crypt.DecryptBuffer(array, 0, array.Length);
            stopwatch.Stop();

            PrintResult("DecryptBuffer", stopwatch);

            /*stopwatch.Reset();
            stopwatch.Start();
            for (int i = 0; i < loopCount; i++)
                crypt.DecryptBufferUnsafe(array, 0, array.Length);
            stopwatch.Stop();

            PrintResult("DecryptBufferUnsafe", stopwatch);*/

            stopwatch.Reset();
            stopwatch.Start();
            for (int i = 0; i < loopCount; i++)
                crypt.Encrypt(array, array.Length);
            stopwatch.Stop();

            PrintResult("Encrypt", stopwatch);

            stopwatch.Reset();
            stopwatch.Start();
            for (int i = 0; i < loopCount; i++)
                crypt.EncryptBuffer(array, 0, array.Length);
            stopwatch.Stop();

            PrintResult("EncryptBuffer", stopwatch);

            stopwatch.Reset();
            stopwatch.Start();
            for (int i = 0; i < loopCount; i++)
                crypt.EncryptBufferUnsafe(array, 0, array.Length);
            stopwatch.Stop();

            PrintResult("EncryptBufferUnsafe", stopwatch);
        }

        static void PrintResult(string resultStr, Stopwatch stopwatch)
        {
            Console.WriteLine("{0}: {1}", resultStr, stopwatch.ElapsedMilliseconds);
        }

    }
}
