﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NxsEmu.Network.GamePackets;

namespace NxsEmu.Game.Gateway
{
    public struct CharacterListEntry
    {
        [Field(64)]
        public ulong characterId;
        [StringField(true)]
        public string characterName;
        [Field(2)]
        public uint characterSex;
        [Field(5)]
        public uint characterRace;
        [Field(5)]
        public uint characterClass;
        [Field(32)]
        public uint characterFaction;
        [Field(32)]
        public uint characterLevel;
        [Field(32)]
        public uint bodyCount;
        [ArrayField("bodyCount"), StructField()]
        public CharacterEquipment[] bodyVisuals;
        [Field(32)]
        public uint equipCount;
        [ArrayField("equipCount"), StructField()]
        public CharacterEquipment[] equipmentVisuals; // NOTE: This is possibly at the end? The packet is weird.


        [Field(32)]
        public uint unk0;
        [Field(32)]
        public uint unk1; // worldID?
        [Field(32)]
        public uint unk2; // realmId?
        [Field(32)]
        public uint unk3; // isntanceID?
        [StructField()]
        public WorldLocation worldLocation;
        [Field(3)]
        public uint charactedPath;
        [Field(1)]
        public uint unk4;
        [Field(32)]
        public uint unk5;

        [Field(4)]
        public uint unkCount;
        [ArrayField("unkCount"), Field(32)]
        public uint[] unkArray0;
        [ArrayField("unkCount"), Field(32)]
        public uint[] unkArray1;

        [Field(32)]
        public uint boneCustomizationCount;
        [ArrayField("boneCustomizationCount"), Field(32)]
        public Single[] boneCustomizations;

        [Field(32)]
        public Single unk6;
    }

    // move elsewhere?
    public struct CharacterEquipment
    {
        [Field(7)]
        public uint itemSlot;
        [Field(32)]
        public uint itemDisplayId;
        [Field(32)]
        public uint itemColorSetId;
        [Field(32)]
        public uint itemDyeData;
    }
}
