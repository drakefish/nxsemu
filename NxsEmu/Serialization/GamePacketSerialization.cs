﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Reflection;
using NxsEmu.Network.GamePackets;

namespace NxsEmu.Serialization
{
    static class GamePacketSerialization
    {
        /* NOTES:
         *  - This is WIP. Some packets will probably make this throw exceptions because of it.
         *  - This needs heavy optimization. It currently takes a few milliseconds to generate some small packet code on my 
         *     machine. We will need to "cache"/re-use/remove as many possible operations as possible so that we can gain as much
         *     perf as possible for the cost of a bit more memory use (that would be worth it).
         *  - TODO: Maybe we could use some special attribute on Reader/Writer methods for basic types. We would then save the methods here.
         *     (that makes it a lot more "type-safe" and readable than what I currently do)
         */

        static GamePacketSerialization()
        {
            // Generate re-usable stuff.

            // Make a static dictionary for packet writer/reader Write methods?
        }

        public static void GenerateGamePacketDelegates<TPacket>(
            out Action<GamePacketWriter, TPacket> writerDelegate,
            out Action<GamePacketReader, TPacket> readerDelegate,
            out Func<TPacket, Int32> sizeDelegate)
            where TPacket : GamePacket<TPacket>, new()
        {
            // TODO: Create an expression tree for each delegate, compile and write to delegate fields.
            // NOTE: Set any unused reference/structure to default (like unions, see under)
            //  - I think we need to make unions use the [FieldOffset] attribs from marshaller.
            //    The way it works currently it uses a lot of space for no reason, especially for big unions.

            var paramPacket = Expression.Parameter(typeof(TPacket), "packet");
            var paramWriter = Expression.Parameter(typeof(GamePacketWriter), "writer");
            var paramReader = Expression.Parameter(typeof(GamePacketReader), "reader");
            var varSize = Expression.Variable(typeof(Int32), "size");

            Expression writerExpression;
            Expression readerExpression;
            Expression sizeExpression;

            GenerateTypeExpressions(typeof(TPacket),
                out writerExpression,
                out readerExpression,
                out sizeExpression, paramPacket,
                paramWriter, paramReader, varSize);

            // make block with variable and add the initial varSize = 0
            sizeExpression = Expression.Block(new ParameterExpression[] { varSize }, Expression.Assign(varSize, Expression.Constant(0)), sizeExpression, varSize);
            writerExpression = Expression.Block(writerExpression, Expression.Call(paramWriter, "FlushByte", null));

            writerDelegate = Expression.Lambda<Action<GamePacketWriter, TPacket>>(writerExpression, paramWriter, paramPacket).Compile();
            readerDelegate = Expression.Lambda<Action<GamePacketReader, TPacket>>(readerExpression, paramReader, paramPacket).Compile();
            sizeDelegate = Expression.Lambda<Func<TPacket, Int32>>(sizeExpression, paramPacket).Compile();

        }

        private static void GenerateTypeExpressions(
            Type structType,
            //string fieldName,
            out Expression writerExpression,
            out Expression readerExpression,
            out Expression sizeExpression,
            //Expression paramPacket,
            Expression exprStruct, // packet, packet.substr1, packet.substr1.substr2, ect. // SET BY CALLER
            Expression paramWriter,
            Expression paramReader,
            Expression varSize)
        {
            List<Expression> outWriterExpressions = new List<Expression>();
            List<Expression> outReaderExpressions = new List<Expression>();
            List<Expression> outSizeExpressions = new List<Expression>();

            FieldInfo[] fields = structType.GetFields();
            foreach (FieldInfo field in fields)
            {
                // Get the non-array type.
                Type t = field.FieldType;
                if (t.IsArray)
                    t = t.GetElementType();

                // Iterator variable for this field. TODO: Initialize it? Or is this done in the loop?
                var varIterator = Expression.Variable(typeof(Int32));

                ArrayFieldAttribute attrArray;
                FieldAttribute attrField;
                StringFieldAttribute attrString;
                StructFieldAttribute attrStruct;
                UnionFieldAttribute attrUnion;

                // Setup the field reference expression dependign on wether or not it's an array.
                Expression fieldRefExpr;
                if ((attrArray = field.GetCustomAttribute<ArrayFieldAttribute>()) != null)
                {
                    // Is this right? We're using the struct array directly here for some reason.

                    fieldRefExpr = Expression.Field(exprStruct, field.Name);
                    fieldRefExpr = Expression.ArrayAccess(fieldRefExpr, varIterator);
                }
                else
                    fieldRefExpr = Expression.Field(exprStruct, field.Name);

                // these are the expressions for field access, looped for an array.
                // todo: can we replace these for blocks instead, simpler syntax.
                //List<Expression> writerExpressions = new List<Expression>();
                //List<Expression> readerExpressions = new List<Expression>();
                Expression fieldWriterExpression;
                Expression fieldReaderExpression;
                Expression fieldSizeExpression;

                if ((attrField = field.GetCustomAttribute<FieldAttribute>()) != null)
                {
                    // TODO: We need to store these methods, we shouldn't have to use such heavy reflection for each and every field..
                    /*fieldWriterExpression = Expression.Call(paramWriter, "WriteValue",
                        new[] { t }, new[] { fieldRefExpr, Expression.Constant(attrField.Bits) });*/
                    // We need to get the right WriteX or ReadX method here. This is most likely temporary.
                    MethodInfo method = typeof(GamePacketWriter).GetMethod("Write" + Type.GetTypeCode(t).ToString());

                    if (method == null)
                        throw new ApplicationException();

                    fieldWriterExpression = Expression.Call(paramWriter, method, fieldRefExpr, Expression.Constant(attrField.Bits));

                    // note: last arg may have to be empty array?
                    /*fieldReaderExpression = Expression.Assign(fieldRefExpr,
                        Expression.Call(paramReader, "ReadValue", new[] { t }, new[] { Expression.Constant(attrField.Bits) }));*/
                    method = typeof(GamePacketReader).GetMethod("Read" + Type.GetTypeCode(t).ToString());
                    fieldReaderExpression = Expression.Assign(fieldRefExpr, 
                        Expression.Call(paramReader, method, Expression.Constant(attrField.Bits)));

                    fieldSizeExpression = Expression.AddAssign(varSize, Expression.Constant(attrField.Bits));
                }
                else if ((attrString = field.GetCustomAttribute<StringFieldAttribute>()) != null)
                {
                    string writeFunc = attrString.Unicode ? "WriteWString" : "WriteString";
                    string readFunc = attrString.Unicode ? "ReadWString" : "ReadString";

                    fieldWriterExpression = Expression.Call(paramWriter, writeFunc, null, new[] { fieldRefExpr });
                    fieldReaderExpression = Expression.Assign(fieldRefExpr, Expression.Call(paramReader, readFunc, null, null));
                    // we need to make sure that the strings stuff is alright, is there any kind of encoding needed to be done?
                    // NOTE: This could be wrong, I'm not completely sure of how .NET strings work exactly.
                    if (attrString.Unicode)
                        fieldSizeExpression = Expression.AddAssign(varSize, 
                            Expression.Multiply(
                            Expression.Multiply(Expression.Property(fieldRefExpr, "Length"), Expression.Constant(2)), Expression.Constant(8)));
                    else
                        fieldSizeExpression = Expression.AddAssign(varSize, 
                            Expression.Multiply(Expression.Property(fieldRefExpr, "Length"), Expression.Constant(8)));
                }
                else if ((attrStruct = field.GetCustomAttribute<StructFieldAttribute>()) != null)
                {
                    Expression exprSubStruct;
                    if (field.FieldType.IsArray)
                        exprSubStruct = Expression.ArrayAccess(Expression.Field(exprStruct, field), varIterator);
                    else
                        exprSubStruct = Expression.Field(exprStruct, field); // is this the right thing?

                    // TODO/NOTE: Should we eventually support classes?

                    GenerateTypeExpressions(t, 
                        out fieldWriterExpression, 
                        out fieldReaderExpression,
                        out fieldSizeExpression,
                        exprSubStruct, paramWriter, paramReader, varSize);
                    // TODO/NOTE: we will possibly change how this works, maybe make it a block.
                }
                else if ((attrUnion = field.GetCustomAttribute<UnionFieldAttribute>()) != null)
                {
                    FieldInfo idField = structType.GetField(attrUnion.TypeFieldName);

                    if (idField == null)
                        Logs.Log(LogType.Compiler, LogLevel.Error, "Packet IO generator failed to get union id field '{0}'", attrUnion.TypeFieldName);

                    // I'm using ifs now. switch didn't work very well..
                    List<Expression> writerExpr = new List<Expression>();
                    List<Expression> readerExpr = new List<Expression>();
                    List<Expression> sizeExpr = new List<Expression>();

                    FieldInfo[] structs = field.FieldType.GetFields();
                    for (uint x = 0; x < structs.Length; x++)
                    {
                        Expression unionStructWriterExpression;
                        Expression unionStructReaderExpression;
                        Expression unionStructSizeExpression;

                        // get struct expressions into our lists
                        Expression exprUnionSubStruct = Expression.Field(Expression.Field(exprStruct, field), structs[x]);
                        GenerateTypeExpressions(
                            structs[x].FieldType,
                            out unionStructWriterExpression,
                            out unionStructReaderExpression,
                            out unionStructSizeExpression,
                            exprUnionSubStruct,
                            paramWriter,
                            paramReader,
                            varSize);

                        writerExpr.Add(
                            Expression.IfThen(
                                Expression.Equal(
                                    Expression.Field(exprStruct, idField),
                                    Expression.Constant(x)),
                                unionStructWriterExpression));

                        readerExpr.Add(
                            Expression.IfThen(
                                Expression.Equal(
                                    Expression.Field(exprStruct, idField),
                                    Expression.Constant(x)),
                                unionStructReaderExpression));

                        sizeExpr.Add(
                            Expression.IfThen(
                                Expression.Equal(
                                    Expression.Field(exprStruct, idField),
                                    Expression.Constant(x)),
                                unionStructSizeExpression));
                    }

                    fieldWriterExpression = Expression.Block(writerExpr);
                    fieldReaderExpression = Expression.Block(readerExpr);
                    fieldSizeExpression = Expression.Block(sizeExpr);
                }
                else
                {
                    Logs.Log(LogType.General, LogLevel.Error, "Encountered unknown field while generating packet IO.");
                    throw new ApplicationException("Encountered unknown field while generating packet IO.");
                }

                if (attrArray != null)
                {
                    // represents the const int or dynamic count field access
                    Expression countExpression;

                    if (attrArray.CountFieldName != null)
                    {
                        FieldInfo countField = structType.GetField(attrArray.CountFieldName);

                        if (countField == null)
                            Logs.Log(LogType.General, LogLevel.Error,
                                "Packet IO generator failed to get array count field '{0}'",
                                attrArray.CountFieldName);

                        countExpression = Expression.Convert(Expression.Field(exprStruct, countField), typeof(Int32));
                    }
                    else
                        countExpression = Expression.Constant(attrArray.StaticCount);


                    LabelTarget label = Expression.Label();

                    //BlockExpression writerBlock = Expression.Block(
                    outWriterExpressions.Add(Expression.Block(
                        new[] { varIterator },
                        Expression.Assign(varIterator, Expression.Constant(0)),
                        Expression.Loop(
                            Expression.IfThenElse(
                                Expression.GreaterThanOrEqual(varIterator, countExpression),
                                Expression.Break(label),
                                Expression.Block(
                                    fieldWriterExpression,
                                    Expression.PostIncrementAssign(varIterator))),
                            label)));

                    //BlockExpression readerBlock = Expression.Block(
                    outReaderExpressions.Add(Expression.Block(
                        new[] { varIterator },
                        Expression.Assign(Expression.Field(exprStruct, field), Expression.NewArrayBounds(t, countExpression)),
                        Expression.Assign(varIterator, Expression.Constant(0)),
                        Expression.Loop(
                            Expression.IfThenElse(
                                Expression.GreaterThanOrEqual(varIterator, countExpression),
                                Expression.Break(label),
                                Expression.Block(
                                    fieldReaderExpression,
                                    Expression.PostIncrementAssign(varIterator))),
                            label)));

                    outSizeExpressions.Add(Expression.Block(
                        new[] { varIterator },
                        Expression.Assign(varIterator, Expression.Constant(0)),
                        Expression.Loop(
                            Expression.IfThenElse(
                                Expression.GreaterThanOrEqual(varIterator, countExpression),
                                Expression.Break(label),
                                Expression.Block(
                                    fieldSizeExpression,
                                    Expression.PostIncrementAssign(varIterator))),
                            label)));
                            
                }
                else
                {
                    // if not array, use the field expressions directly.
                    outWriterExpressions.Add(fieldWriterExpression);
                    outReaderExpressions.Add(fieldReaderExpression);
                    outSizeExpressions.Add(fieldSizeExpression);
                }
            }
            
            writerExpression = outWriterExpressions.Count != 0 ? 
                (Expression)Expression.Block(outWriterExpressions) : Expression.Empty();
            readerExpression = outReaderExpressions.Count != 0 ?
                (Expression)Expression.Block(outReaderExpressions) : Expression.Empty();
            sizeExpression = outSizeExpressions.Count != 0 ?
                (Expression)Expression.Block(outSizeExpressions) : Expression.Constant(0);


            //readerExpression = Expression.Block(outReaderExpressions);
            //sizeExpression = Expression.Block(outSizeExpressions);
        }
    }
}
