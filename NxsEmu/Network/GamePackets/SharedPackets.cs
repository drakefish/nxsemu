﻿using System;

namespace NxsEmu.Network.GamePackets
{
    [PacketDescription(GamePacketMsg.State)]
    public class State : GamePacket<State>
    {
        [Field(8)]
        public Byte state;
    }

    [PacketDescription(GamePacketMsg.State2)]
    public class State2 : GamePacket<State2>
    {
        [Field(8)]
        public Byte state;
    }

    [PacketDescription(GamePacketMsg.SHello)]
    public class SHello : GamePacket<SHello>
    {
        [Field(32)] public uint buildNumber;
        [Field(32)] public uint realmId;
        [Field(32)] public uint realmGroupId;
        [Field(32)] public uint realmGroupEnum;
        [Field(64)] public ulong startupTime;
        [Field(16)] public ushort listenPort;
        [Field(5)] public uint connectionType;
        [Field(32)] public uint networkMessageCRC;
        [Field(32)] public uint processId;
        [Field(64)] public ulong processCreationTime;
        // there's a new field now.
        [Field(32)] public uint unk;
    }

}
