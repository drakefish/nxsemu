﻿using System;
using System.Text;
using System.IO;
using System.Runtime.CompilerServices;

namespace NxsEmu.Network.GamePackets
{
    public class GamePacketWriter : IDisposable
    {
        private const Int32 BYTESIZE = 8;

        //private BinaryWriter m_binaryWriter;
        
        private readonly Stream m_stream;
        private readonly Boolean m_leaveOpen;
        private Boolean m_disposed;
        private Byte m_partialByte; // byte being written.
        private Int32 m_bitsInPartialByte; // number of bits used

        private GamePacketWriter() { }
        public GamePacketWriter(Stream stream, Boolean leaveOpen = false)
        {
            if (stream == null)
                throw new ArgumentNullException();
            if (!stream.CanWrite)
                throw new InvalidOperationException("The stream must be writeable.");

            // TODO: Make sure stream's buffer is accessible (?), ect.
            //m_binaryWriter = new BinaryWriter(stream);
            m_stream = stream;
            m_leaveOpen = leaveOpen;
            m_partialByte = 0;
            m_bitsInPartialByte = 0;
        }

        public Stream BaseStream
        {
            get { return m_stream; }
        }

        protected Byte PartialByte
        {
            get { return m_partialByte; }
            set { m_partialByte = value; }
        }

        protected int BitsInPartialByte
        {
            get { return m_bitsInPartialByte; }
            set { m_bitsInPartialByte = value; }
        }
        
        /* I want the WriteX methods to be inlined by the CLR, the goal would be
         *  that it becomes a simple size check and a call to the main write func.
         */

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe void WriteSByte(SByte value, Int32 bits = 8)
        {
            if (bits > sizeof(SByte) * BYTESIZE)
                throw new ArgumentOutOfRangeException();

            UInt32 _value = *(UInt32*)&value;
            WriteUInt32(_value, bits);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe void WriteByte(Byte value, Int32 bits = 8)
        {
            if (bits > sizeof(Byte) * BYTESIZE)
                throw new ArgumentOutOfRangeException();

            UInt32 _value = *(UInt32*)&value;
            WriteUInt32(_value, bits);
        }

        public void WriteBoolean(Boolean value, Int32 bits = 1)
        {
            if (bits > sizeof(UInt32) * BYTESIZE)
                throw new ArgumentOutOfRangeException();

            WriteUInt32Unchecked(value == false ? 0 : (uint)1, bits);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe void WriteInt16(Int16 value, Int32 bits = 16)
        {
            if (bits > sizeof(Int16) * BYTESIZE)
                throw new ArgumentOutOfRangeException();

            UInt32 _value = *(UInt32*)&value;
            WriteUInt32Unchecked(_value, bits);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe void WriteUInt16(UInt16 value, Int32 bits = 16)
        {
            if (bits > sizeof(UInt16) * BYTESIZE)
                throw new ArgumentOutOfRangeException();

            UInt32 _value = *(UInt32*)&value;
            WriteUInt32Unchecked(_value, bits);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe void WriteInt32(Int32 value, Int32 bits = 32)
        {
            if (bits > sizeof(Int32) * BYTESIZE)
                throw new ArgumentOutOfRangeException();

            UInt32 _value = *(UInt32*)&value;
            WriteUInt32Unchecked(_value, bits);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteUInt32(UInt32 value, Int32 bits = 32)
        {
            if (bits > sizeof(UInt32) * BYTESIZE)
                throw new ArgumentOutOfRangeException();

            WriteUInt32Unchecked(value, bits);
        }

        /* This is the main and regular writing function.
         * It will write the value and bits count without doing any kind of check on them.
         */
        private void WriteUInt32Unchecked(UInt32 value, Int32 bits)
        {
            // check for disposed here instead of the publicly exposed writers.
            if (m_disposed)
                throw new ObjectDisposedException("GamePacketWriter");

            while (bits > 0)
            {
                Int32 remainingBits = 8 - BitsInPartialByte;
                if (remainingBits > bits)
                    remainingBits = bits;
                PartialByte |= (Byte)((value & ((1 << remainingBits) - 1)) << BitsInPartialByte);
                value >>= remainingBits;
                bits -= remainingBits;
                BitsInPartialByte = (remainingBits + BitsInPartialByte) & 7;

                // flush the byte to buffer.
                if (BitsInPartialByte == 0)
                    WritePartialByte();

                    // The partial byte is full, flush it? (call flush eventually)
                    //Writer.Write(m_partialByte);
                    //m_partialByte = 0;
            }
        }

        /*
         * Important note:
         *  I think that some (maybe not all) of the Int64 (and possibly others) values have special rules applied to them.
         *  We will have to check this out, we may have to add attribute info or just do extra maths on these.
         */

        public unsafe void WriteInt64(Int64 value, Int32 bits = 64)
        {
            if (bits > sizeof(Int64) * BYTESIZE)
                throw new ArgumentOutOfRangeException();

            UInt64 _value = *(UInt64*)&value;
            WriteUInt64Unchecked(_value, bits);
        }

        public unsafe void WriteUInt64(UInt64 value, Int32 bits = 64)
        {
            if (bits > sizeof(UInt64) * BYTESIZE)
                throw new ArgumentOutOfRangeException();

            WriteUInt64Unchecked(value, bits);
        }

        private void WriteUInt64Unchecked(UInt64 value, Int32 bits)
        {
            if (m_disposed)
                throw new ObjectDisposedException("GamePacketWriter");

            while (bits > 0)
            {
                Int32 remainingBits = 8 - BitsInPartialByte;
                if (remainingBits > bits)
                    remainingBits = bits;
                PartialByte |= (Byte)((value & (UInt64)((1 << remainingBits) - 1)) << BitsInPartialByte);
                value >>= remainingBits;
                bits -= remainingBits;
                BitsInPartialByte = (remainingBits + BitsInPartialByte) & 7;

                // flush the byte to buffer.
                if (BitsInPartialByte == 0)
                    WritePartialByte();
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe void WriteSingle(Single value, Int32 bits = 32)
        {
            if (bits > sizeof(Single) * BYTESIZE)
                throw new ArgumentOutOfRangeException();
            
            UInt32 intValue = *(UInt32*)&value;
            WriteUInt32Unchecked(intValue, bits);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe void WriteDouble(Double value, Int32 bits = 64)
        {
            if (bits > sizeof(Double) * BYTESIZE)
                throw new ArgumentOutOfRangeException();

            UInt64 intValue = *(UInt64*)&value;
            WriteUInt64Unchecked(intValue, bits);
        }

        public void WriteBytesArray(Byte[] bytes)
        {
            if (bytes == null)
                throw new ArgumentNullException();
            if (bytes.Length <= 0)
                return;

            for (int i = 0; i < bytes.Length; i++)
                WriteByte(bytes[i]);
        }

        /*public void WriteString(string value)
        {
            // Allow null strings.
            value = value ?? String.Empty;
            
        }*/

        public void WriteWString(string value)
        {
            // accept null strings, make them an empty string.
            value = value ?? String.Empty;
            Byte[] bytes = Encoding.Unicode.GetBytes(value);
            Int32 len = bytes.Length / 2;

            // This is unlikely to happen but it's invalid if it does.
            if (len > 32767)
                throw new ArgumentOutOfRangeException("The string length is too big for the packet.");

            // NOTE: this could need a bit of tweaking.
            int bits = (len > 127 ? 15 : 7);
            WriteInt32((len > 127 ? 1 : 0), 1);
            WriteInt32(len, bits); // if there are more than 127 chars, len is 15 bits.
            WriteBytesArray(bytes);
        }

        /// <summary>
        /// Flushes the current partial byte to the stream, if any.
        /// </summary>
        public void FlushByte()
        {
            if (m_disposed)
                throw new ObjectDisposedException("GamePacketWriter");

            //Do we need to do something special when it's not full? I guess not.. Just write the byte and reset value
            //if (BitsInPartialByte != 0)
            //
            // Only flush if there currently are bits in the partial byte.
            if (BitsInPartialByte != 0)
                WritePartialByte();
        }

        // I currently have issues with the partial byte, it's not used as I think it is? I need to check it out so that flush
        //  can work as expected at all time.
        private void WritePartialByte()
        {
            if (m_disposed)
                throw new ObjectDisposedException("GamePacketWriter");

            BaseStream.WriteByte(PartialByte);
            PartialByte = 0;
            BitsInPartialByte = 0;
        }

        // should dispose call FlushByte? Is it its job?
        public void Dispose()
        {
            if (!m_disposed && !m_leaveOpen && BaseStream != null)
                BaseStream.Dispose();

            m_disposed = true;
        }
    }
}
