﻿using System;

namespace NxsEmu.Network.GamePackets
{
    [AttributeUsage(AttributeTargets.Class)]
    public class PacketDescriptionAttribute : Attribute
    {
        public GamePacketMsg Id { get; set; }
        public PacketDescriptionAttribute(GamePacketMsg id) { Id = id; }
    }
}
