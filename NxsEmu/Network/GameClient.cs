﻿using System;
using System.IO;
using System.Net.Sockets;
using NxsEmu.Cryptography;
using NxsEmu.Network.GamePackets;

namespace NxsEmu.Network
{
    public abstract class GameClient : BasicClient
    {
        // TODO: Put these somewhere else? In the GamePacket class maybe?
        private const int PACKET_SIZE_BITS = 0x18; // probably won't change.
        private const int PACKET_SIZE_BYTES = (PACKET_SIZE_BITS + 7) / 8;
        private const int PACKET_ID_BITS = 0xC; // has changed from 0xB over time
        private const int PACKET_ID_BYTES = (PACKET_ID_BITS + 7) / 8;
        private const int PACKET_HEADER_BITS = PACKET_SIZE_BITS + PACKET_ID_BITS;
        private const int PACKET_HEADER_BYTES = (PACKET_HEADER_BITS + 7) / 8;

        private PacketCrypt m_packetCrypt;

        public GameClient(Socket socket, PacketCrypt packetCrypt) 
            : base(socket) 
        {
            if (socket == null)
                throw new ArgumentNullException();

            m_packetCrypt = packetCrypt;
        }

        // function is currently working properly.
        public void SendGamePacket(GamePacket packet)
        {
            if (packet == null)
                throw new ArgumentNullException();

            // Total msg size.
            Int32 pktSize = PACKET_HEADER_BYTES + packet.GetPackedByteSize();

            using (MemoryStream stream = new MemoryStream(pktSize))
            using (GamePacketWriter writer = new GamePacketWriter(stream))
            {
                writer.WriteInt32(pktSize, PACKET_SIZE_BITS);
                writer.WriteUInt32((UInt32)packet.PacketId, PACKET_ID_BITS);
                packet.WriteTo(writer);
                // test
                //Logs.Log("pkt {0}:", packet.PacketId);
                //Logs.LogBuffer(stream.GetBuffer());
                SendMessage(stream);
            }
        }

        // works, todo: allow sending multiple packets in a single one?
        public void SendGamePacketWrapped(GamePacket packet)
        {
            if (packet == null)
                throw new ArgumentNullException();

            Int32 packedSize = packet.GetPackedByteSize();
            Int32 bufferSize = packedSize + (PACKET_HEADER_BYTES * 2);
            
            using (MemoryStream stream = new MemoryStream(bufferSize + 1))
            using (GamePacketWriter writer = new GamePacketWriter(stream))
            {
                // Wrapper packet header.
                writer.WriteInt32(bufferSize, PACKET_SIZE_BITS);
                writer.WriteUInt32((UInt32)WrapperMsgId, PACKET_ID_BITS);

                // Skip any remaining bits.
                writer.FlushByte();

                // Wrapped packet header.
                writer.WriteInt32(packedSize + PACKET_HEADER_BYTES, PACKET_SIZE_BITS);
                writer.WriteUInt32((UInt32)packet.PacketId, PACKET_ID_BITS);
                packet.WriteTo(writer);

                // flush any remaining bits. (WriteTo() should flush? Or not because it shouldn't worry about the output)
                writer.FlushByte();

                // Wrapped packet encryption
                PacketCrypt.EncryptBuffer(stream.GetBuffer(), 8, packedSize + PACKET_ID_BYTES);

                //Logs.LogBuffer(stream.GetBuffer());

                SendMessage(stream);
            }
        }

        /// <summary>
        /// Gets or sets the PacketCrypt instance used to crypt communication with this client.
        /// </summary>
        public PacketCrypt PacketCrypt
        {
            get { return m_packetCrypt; }
            set { m_packetCrypt = value; }
        }

        protected abstract GamePacketMsg WrapperMsgId { get; }
    }
}
