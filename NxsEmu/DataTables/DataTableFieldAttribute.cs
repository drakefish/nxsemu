﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NxsEmu.DataTables
{
    [AttributeUsage(AttributeTargets.Field)]
    public class DataTblFieldAttribute : Attribute
    {
        public Int32 Offset { get; set; }
        public Int32 MaxLen { get; set; } // for strings
        public Boolean Unused { get; set; }

        public DataTblFieldAttribute(int offset)
        {
            Offset = offset;
        }
    }
}
