﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NxsEmu.DataTables
{
    public static class DataTable<TStructure> 
        where TStructure : DataTableEntry<TStructure>
    {
        private static TStructure[] s_loadedStructures;

        // static ctor for this loads the table. (or some init)
        // nothing derives from this, it's static and a unique static type is created for each DataTableStructure-derived types
        //  that are passed to this.
    }
}
